import logging


logger = logging.getLogger()

logging.basicConfig(format='\n%(asctime)s - scallop / %(name)s - %(levelname)s - %(message)s')
logger.setLevel(logging.INFO)



