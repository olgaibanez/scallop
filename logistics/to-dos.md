# TO-DO list
In this document we will include ideas for improvement of scallop.
Some ideas will be reflected as changes in the code, and others as
changes in 

## Next step
- [ ] Find best resolution [Alex]
- [ ] Study local optimal resolutions, i.e. given a resolution, find near 
      resolutions with better clustering scores. [Alex]



## IMPORTANT STUFF (must be done)
### Plotting
- [ ] Multiple figures 
- [ ] Add colorbar in plot score
- [ ] Add contour plots in plotScore -> See wild idea XOXO

### Cases of use
- [ ] Subclustering
- [ ] Graph representation

### Programming and features
- [ ] Change bt.ident as following: if not ident is given, 
      take it from adata.obs['leiden']; if it does not exist, create one.
- [ ] Extend clustering to other techniques (kmeans)
- [ ] Clean `nanmatrix` approach for `Bootstrap._freqScore()`.
- [ ] Apply inherited attributes to `Bootstrap()` object 
      (see https://stackoverflow.com/a/8854089)
- [ ] Apply CPU and memory profiling of `sl.tl.getScore()` to find 
      improvable steps.
- [ ] Try modin module to speed up pandas processes supposedly only change
      ``import pandas as pd`` to ``import modin.pandas as pd``
- [ ] I/O of scallop objects as hdf5 or loom files.
- [ ] Make tqdm appear as logging.info
- [ ] Integrate a comprenhensive set of datasets.

### Tests and comprobations
- [ ] Analyze how overlap score between unknown clusters is affected by 
      ``frac_cells`` or the size of the cluster. Test it in a toy dataset 
      with two 100-cell clusters. 
- [ ] See what is the effect of ``frac_cells`` on the system. For that, 
      we can take several ``frac_cells`` (1, 0.95, 0.85), and obtain the
      scores (mean freqScore) for several resolutions (to find resolutions 
      where two clusters are being divided). Then, for each resolution,
      compare which cells have highest score variance across ``frac_cells``,
      and infer about cluster size, location, or any other stuff. 
      If possible, find the ``frac_cells`` that, in cells with high 
      variance across ``frac_cells``, yields higher scores for stable 
      regions (already divided clusters, or clusters that are divided) 
      and lower scores for unstable regions (cells that belong 
      to several clusters always, or once two clusters are already 
      determined).      

      
##  WILD IDEAS (nothing happens if we never get to do them)
- [ ] Wild idea: somehow use info of mapping_matrix from munkres (maybe get a new evaluation score, like ratio of
       elements in nonzer/elements in zero)
- [ ] Calculate the number of trials automatically to avoid having cells with all trials as empty values.
      If f is the fraction of cells, n is the number of trials, c is the number of cells and X is the number of cells
      to expect having all empty (we would expect X = 0.1 - 0.5), then the following expression relates all elements
      ```math
      (1-f)^nc = X
      ```
      Then, we solve for n to obtain the number of trials
      ```math
      n = log(X/c)/log(1-f)
      ```.
      Generally, for big number of cells and high frac_cells, the number
      of iterations is low (2-7). In those cases we can choose a
      somewhat high number. We should also discuss if selecting the number
      based on obtaining X cells with less than half their trials empty,
      which is more mathematically complex, but more interesting to apply.
- [ ] Cluster assignment tree: for each k or res, show a tree structure
      of how clusters are divided, i.e., cluster A is divided into B and
      C at res(0.9), then B into D and E at 1.2, and plot those results
      as a tree. (One axis is the resolution time and the other is a
      representation of clusters).
- [ ] Improve scanpy's blob generation with personal function that creates
      subblobs and other parafernalia (much to be refined).
- [ ] XOXO. In the HCA talk Dana Peer showed that clusters with subclusters
      appeared as density peaks at the contour plot. Will those density 
      peaks be associated with dividing clusters and reduced scores?
      
      
## Related stuff (Gitlab, management, etc.)
- [ ] Change testing and adapt it to the GitLab CI/CD runner (because now
      if a test fails, since the system works fine, the overall test run 
      may turn out right even if a test failed).
- [ ] Avoid load cache for ``coverage`` stage, and avoid to save cache 
      for ``test`` stage.



## Done
- [x] Implement ``force`` for ``sl.tl.getScore()`` [Alex]
- [x] Parallelize clustering [Alex]
- [x] IDs for bootstraps (we discard the hashtags at this moment)
- [x] Apply logger [Alex]
- [x] Extend clustering to other techniques (louvain)
- [x] Apply correction for newly mapped clusters.
      This correction should look for cluster numbers that are the same
      and rename them to be the same.
      This might affect the final freqScore.
- [x] Study convergence of `sl.tl.getScore()` [Alex] 
- [x] We believe some datasets show 2 or 3 different clusterings with 
      frac_cells=1. To test that, we will run leiden X times with all cells
      and run the ARI / similarity metrics between all cases, and 
      plot a heatmap of similarity. [Alex]
      **Resolution**: We have studied the variability, and the results 
      can be found in ``scallop-notebooks``.

      
      


## Discarded ideas / negative results (Explain why they were discarded)
- [x] Study for each k, which is the resolution that best explains the
      number of clusters. [Olga] **Resolution**: So far the study has 
      diverted. Might be recoverable in the future.
- [x] Add Interscore. **Resolution**: Much stuff is being done in freqScore, and it is 
      a really customizable score that might introduce more unwanted 
      variability than information about the system. If we get to 
      understand the biology of the system, might be recoverable.
- [x] Get consensus clustering (already done but need function to retrieve it).
      **Resolution**: We have found such heterogeneity on the clustering 
      solutions that finding the consensus clustering can obscure the 
      clusters at that resolution. Also, we think that users want to 
      see the effects on THEIR clustering, and might be discouraged by
      a secondary clustering solution (might not be the case, we don't 
      know). Score does indeed reflect events of clustering division, 
      so this *consensus* clustering can be inferred from the score if
      the user gets to know how to read it.
- [x] If one cluster for ident is subclustered in two in one of the 
bootstrap solutions, what about tagging both as unknown and merging 
them under a single cluster name. **Resolution**: It is highly likely
it will be a splitting event, which we want to study.


      
