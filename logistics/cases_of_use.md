# Cases of use
In this document we will include the main cases of use for each version.

### Get robustness score per cell (added v1.0)
User has an annData object with a single-cell RNA seq dataset. 
She has run Leiden/Louvain to detect cell populations within the dataset 
and wants to know whether the clustering solution obtained is robust. 
Scallop runs *n\_trial* runs of a bootstrap using a percentage *per\_cell* 
of cells each time, and computes a cell-wise robustness score. 
This robustness score is written into the original annData object provided by the user, 
but can also be exported as a .csv file. Scallop will contain plotting 
tools in order to allow the user represent the robustness score on several 
dimensionality reductions (PCA, UMAP, t_SNE, PHATE). Two
main robustness scores will be available:

* freqScore: frequency of the most common cluster assigned to each cell 
(after clusters have been mapped across bootstrap iterations)
* interScore: definition to be added by Alex


```python 
import scanpy as sc
import scallop as sl
adata = sc.read('filename.h5ad')
scal = sl.Scallop(adata) # Scallop object is initialized from annData object

sl.tl.getScore(scal, clustering='leiden', res=0.3, score_type='freqScore') #score is saved into scal object
freq = sl.tl.getScore(scal, clustering='leiden', res=0.3, score_type='freqScore', do_return=True) #score is returned
```



