# SINCE THE IMAGE WIEGTHS A LOT (1.3 GB approx) I RECOMMEND DELETING THE DOCKER REPO
# OR OVERRIDE THE COMMIT TO UPLOAD A NEW IMAGE.

dir_scallop=$(dirname $(dirname $(readlink -f "$0")))
scallop_file='/docker/install_scallop.sh'
scallop_sh="${dir_scallop}${scallop_file}"
echo $dir_scallop
echo $scallop_sh

sudo docker rm $(sudo docker ps -a -q)
sudo docker rmi $(sudo docker images -q)
sudo docker run -it -v $dir_scallop:/home continuumio/miniconda3 /bin/bash -c "apt update && apt install -y build-essential && apt install -y libxml2-dev zlib1g-dev &&  pip install -r /home/requirements.txt"

sudo docker commit -m "Updated image" $(sudo docker ps -a | head -n2 | tail -n1 | cut -f 1 -d ' ') alexmascension/scallop
sudo docker push alexmascension/scallop
