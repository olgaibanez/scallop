Changelog
==================

v1.0
----
Main components of ``scallop``:
 * ``sl.Scallop`` and ``sl.Bootstrap`` objects.
 * tl: ``sl.tl.getScore()`` .
 * pl: ``sl.pl.plotScore()``.
 * datasets: ``sl.datasets.delete_datasets()`` and ``sl.datasets.joost2016()``.


v1.0.1
------
Fixed pip dependencies.

v1.0.2
------
Corrected calculation of ``freqScore`` to range between 0 and 1.

v1.0.3
------
Fixed minor issues.

v1.0.4
------
Corrected scallop clustering ``use_weights`` parameter inferred from scanpy's.

v1.1.0
------
* Added logging support.
* Implemented parallelization of ``sl.tl.getScore()``.
* Added ``louvain`` to clustering algorithms.
* Added 10x mouse brain and heart 10k datasets.
* Remapping of unknown clusters.

v1.1.1
------
Corrected error on sampling for bootstrap part of  ``sl.tl.getScore()``.
When bootstrapping, a set of random indices are selected. If ``frac_cells=1``
then all indices are selected. However, those indices were randomized, and
even with the same ``seed`` value, clustering would yield different results.
Fixing the issue implies to sort the selected indices, regardless of ``frac_cells``
selected.

v1.2.0
------
* Implemented ``force`` option for ``sl.tl.getScore()``.
* Applied some assertions on ``sl.tl.getScore()``.
* Added dictionary of arguments to add seed to parallel calculation of ``bt_matrix``.
* Added ``seed_in_bt`` as argument for ``sl.tl.getScore()``.
* Corrected ``seed_in_bt`` to solve ``None`` problem from leiden. In some datasets, if seed
  is None, the random generation algorithm only considers 6-or-so different seeds.

v1.3.0
------
* Ensure compatibility with latest Scanpy AnnData structure (v1.8).
* Complete the documentation.
* Ensure compatibility with ray latest version.
* Minor fix in nan management during freqScore calculation.