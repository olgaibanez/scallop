Installation guide
==================

``scallop`` can be installed via PyPi::

    pip install scallop
    
    
    
Development version
-------------------

If you want to install the latest development version you can do it by cloning the 
repository::

    git clone https://gitlab.com/olgaibanez/scallop.git
    
Do ``cd`` there::
    
    cd scallop
    
Checkout to ``dev`` branch::
    
    git checkout dev
    
And run pip in "editable mode"::

    pip install -e .

    
