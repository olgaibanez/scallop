API
=======

scallop functions
-----------------

scallop.pl 
++++++++++

.. autofunction:: scallop.pl.plotScore

scallop.tl 
++++++++++

.. autofunction:: scallop.tl.getScore


scallop.datasets
++++++++++++++++

.. autofunction:: scallop.datasets.delete_datasets
.. autofunction:: scallop.datasets.joost2016
.. autofunction:: scallop.datasets.neurons10k
.. autofunction:: scallop.datasets.heart10k

scallop classes
-----------------

.. autoclass:: scallop.Bootstrap
   :members:
   
.. autoclass:: scallop.Scallop
   :members:
